Lanterna
=================
Overview
---------------
A java curses implementation for textual window interfaces.

```shell
chromium-browser https://github.com/mabe02/lanterna
```
