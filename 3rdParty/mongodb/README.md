MongoDB
=================
Overview
---------------
MongoDB is a document-oriented database that is free and
open-source. It is classified as a NoSQL database because it does not rely on a
traditional table-based relational database structure. Instead, it uses
JSON-like documents with dynamic schemas. Unlike relational databases, MongoDB
does not require a predefined schema before you add data to a database. You can
alter the schema at any time and as often as is necessary without having to
setup a new database with an updated schema.

Provision
---------------
```shell
docker run -it -h mongodb --name mongodb -v /tmp/mongodb:/tmp centos
cat <<'EOF' >/etc/yum.repos.d/mongodb-org.repo
[mongodb-org-3.4]
name=MongoDB Repository
baseurl=https://repo.mongodb.org/yum/redhat/$releasever/mongodb-org/3.4/x86_64/
gpgcheck=1
enabled=1
gpgkey=https://www.mongodb.org/static/pgp/server-3.4.asc
EOF
yum repolist
yum install -y mongodb-org
sed -in -e 's#bindIp:.*$#bindIp: 0.0.0.0#' /etc/mongod.conf
/usr/bin/mongod -f /etc/mongod.conf
```

Exercise
---------------
```shell
# connect to the database
mongo

# what can be done?
db.listCommands()
db.help()
db.users.find().help() # append ".help()" for usage

# create a new database
use khallware
show dbs

# create a new user
db.users.insert({username : "webapp", password : "webapp"})
db.users.find()

# add data
db.mytable.insert({key : "mykey1", value : "myvalue1"})
db.mytable.find()
db.mytable.insert({key : "mykey2", value : "myvalue2"})
db.mytable.find()

# indexes
db.mytable.ensureIndex({ "key" : 1})
db.mytable.dropIndex({ "key" : 1})
```

Java PoC
---------------
```shell
mkdir -p /tmp/mongo/
cp ~/tmp/jdk-8u181-linux-x64.rpm /tmp/mongo/
git clone https://gitlab.com/harkwell/java-poc.git /tmp/mongo/java-poc
docker run -it -h mongo --name mongo -v /tmp/mongo:/tmp --link mongodb centos
yum install -y /tmp/jdk-8u181-linux-x64.rpm

# maven
yum install -y wget
wget -c 'http://mirrors.ibiblio.org/apache/maven/maven-3/3.6.0/binaries/apache-maven-3.6.0-bin.tar.gz' -O /tmp/maven.tgz
mkdir -p /usr/local/maven
tar zxvf /tmp/maven.tgz -C /usr/local/maven --strip-components=1
export PATH=$PATH:/usr/local/maven/bin
export JAVA_HOME=$(rpm -ql jdk1.8 |grep javac$ |sed 's#/bin/javac##')
cd /tmp/java-poc/3rdParty/mongodb/
export POC_MAVEN_REPO=/tmp/foo
mvn -Dmaven.repo.local=$POC_MAVEN_REPO package
java -jar target/mongo-poc-0.0.1.jar
```
