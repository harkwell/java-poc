package com.khallware.mongo;

import com.mongodb.DB;
import com.mongodb.MongoClient;
import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;

public class Main
{
	public static void main(String... args) throws Exception
	{
		MongoClient client = new MongoClient("mongodb",27017);
		DB db = client.getDB("khallware");

		/* if (!db.authenticate("webapp","webapp".toCharArray())) {
			throw new IllegalStateException("mongo auth failure");
		} */
		// show dbs
		System.out.printf("mongo: show dbs");
		client.getDatabaseNames().forEach(System.out::println);

		// db.mytable.find()
		String name = "mytable";
		DBCollection collection = db.getCollection(name);
		System.out.printf("mongo: db.%s.find()\n", name);
		DBCursor cursor = collection.find(new BasicDBObject());

		while (cursor.hasNext()) {
			System.out.println(cursor.next());
		}
	}
}
