Apache Shiro
=================
Overview
---------------
Shiro provides a framework for role based authorization in java.  The
footprint is pretty heavy with over 900 classes.  It can be used in
conjunction with LDAP.

```shell
chromium-browser https://shiro.apache.org/
```

Build
---------------

```shell
SHIRO_MAVEN_REPO=/tmp/foo
rm -rf $SHIRO_MAVEN_REPO
mvn -Dmaven.repo.local=$SHIRO_MAVEN_REPO package
```

Run
---------------
Plain text file example:

```shell
cat <<'EOF' >/tmp/shiro.ini
[users]
khall = khall, teacher
foo1 = foo1, grade0
foo2 = foo2, grade1
foo3 = foo3, grade2

[roles]
teacher = *
grade0 = addition
grade1 = subtraction
grade1 = multiplication, division
EOF
java -jar target/myshiro-poc-0.1.jar khall khall add 1 4 6 4
```


Open Ldap example:

```shell
ORIGDIR=$PWD
cd /tmp && git clone https://gitlab.com/harkwell/devops-poc.git
cd devops-poc/ldap
docker run -it --name ldap -h ldap -v $PWD:/tmp centos
yum install -y openlap openldap-servers openldap-clients which
export PASSWD=mypassword
cd /tmp && bash init.sh $PASSWD
/usr/sbin/slapd -d 256 -F /etc/openldap/slapd.d
#docker exec -it ldap bash
bash setup.sh $PASSWD
bash add-ldap-user.sh khall "Kevin D. Hall" "somepasswd" $PASSWD
IPADDR=$(docker inspect ldap |grep IPAddress |tail -1 |cut -d\"  -f4)
ldapsearch -x -H "ldap://$IPADDR/" -LLL -b dc=khallware,dc=com -D cn=Manager,dc=khallware,dc=com -w $PASSWD

# no workie.... org.apache.shiro.authc.AuthenticationException

cat <<-EOF >/tmp/shiro.ini
[main]
realm = org.apache.shiro.realm.ldap.DefaultLdapRealm
realm.userDnTemplate = uid={0},ou=users,dc=khallware,dc=com
realm.contextFactory.url = ldap://$IPADDR:389
realm.contextFactory.authenticationMechanism = SIMPLE
realm.contextFactory.environment[ldap.searchBase] = ou=Users,dc=khallware,dc=com

securityManager.realms = \$realm, \$realm


[roles]
teacher = *
grade0 = addition
grade1 = subtraction
grade1 = multiplication, division
EOF
cat <<'EOF' >/tmp/log4j.properties
log4j.rootLogger=TRACE, A1
log4j.appender.A1=org.apache.log4j.ConsoleAppender
log4j.appender.A1.layout=org.apache.log4j.PatternLayout
log4j.appender.A1.layout.ConversionPattern=%c %-5p - %m%n
EOF
java -Dlog4j.configuration=file:/tmp/log4j.properties -jar target/myshiro-poc-0.1-jar-with-dependencies.jar khall somepasswd add 1 4 6 4
```
