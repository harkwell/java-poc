Java Native Access
=================
Overview
---------------
Better implementation of Java Native Interface that requires no java C header
files.

```shell
chromium-browser https://github.com/java-native-access/jna
```
