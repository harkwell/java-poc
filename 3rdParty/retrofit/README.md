Retrofit (by Square)
=================
Overview
---------------
An android HTTP client that is thread safe.

```shell
chromium-browser https://square.github.io/retrofit/
```

* Utilize a java interace to map an external RESTful service to.
