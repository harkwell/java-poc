package com.khallware.kafka;

import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import java.util.Arrays;

public class Main
{
	private static KafkaConsumer<String, String> consumer =
		new KafkaConsumer(System.getProperties());

	public static void main(String... args) throws Exception
	{
		consumer.subscribe(Arrays.asList("khallware"));

		while (true) {
			consumer.poll(100).forEach(System.out::println);
			/*	System.out.printf("key = '%s', val = '%s'\n",
					record.key(), record.value());
			} */
		}
	}
}
