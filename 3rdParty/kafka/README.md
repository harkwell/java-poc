Apache Kafka
=================
Overview
---------------
Per Apache's website: "Kafka is used for building real-time data pipelines and
streaming apps. It is horizontally scalable, fault-tolerant, wicked fast, and
runs in production in thousands of companies."

Java PoC
---------------
```shell
screen
mkdir -p /tmp/kafka/ && cp ~/tmp/jdk-8u181-linux-x64.rpm /tmp/kafka/
chromium-browser https://kafka.apache.org/downloads

# .............................................................................
docker run -it -h kafka --name kafka -v /tmp/kafka:/tmp centos
yum install -y wget
wget -c 'http://www-us.apache.org/dist/kafka/2.1.0/kafka_2.11-2.1.0.tgz' -O /tmp/kafka.tgz
mkdir -p /usr/local/kafka && tar xvf /tmp/kafka.tgz -C /usr/local/kafka --strip-components=1
yum install -y /tmp/jdk-8u181-linux-x64.rpm
export PATH=$PATH:/usr/local/kafka/bin
export JAVA_HOME=$(rpm -ql jdk1.8 |grep javac$ |sed 's#/bin/javac##')
cd /usr/local/kafka
zookeeper-server-start.sh config/zookeeper.properties &
kafka-server-start.sh config/server.properties &

# ctrl-a c
# .............................................................................
mkdir -p /tmp/kafka-client/
cp ~/tmp/jdk-8u181-linux-x64.rpm /tmp/kafka-client/
git clone https://gitlab.com/harkwell/java-poc.git /tmp/kafka-client/java-poc
docker run -it -h kclient --name kclient -v /tmp/kafka-client:/tmp --link kafka centos
yum install -y /tmp/jdk-8u181-linux-x64.rpm

# maven
yum install -y wget
wget -c 'http://mirrors.ibiblio.org/apache/maven/maven-3/3.6.0/binaries/apache-maven-3.6.0-bin.tar.gz' -O /tmp/maven.tgz
mkdir -p /usr/local/maven
tar zxvf /tmp/maven.tgz -C /usr/local/maven --strip-components=1
export PATH=$PATH:/usr/local/maven/bin
export JAVA_HOME=$(rpm -ql jdk1.8 |grep javac$ |sed 's#/bin/javac##')
cd /tmp/java-poc/3rdParty/kafka/
export POC_MAVEN_REPO=/tmp/foo
mvn -Dmaven.repo.local=$POC_MAVEN_REPO package

# ctrl-a n
# .............................................................................
# create a topic
kafka-topics.sh --create --zookeeper localhost:2181 --replication-factor 1 --partitions 1 --topic khallware

# list topics
kafka-topics.sh --list --zookeeper localhost:2181

# ctrl-a n
# .............................................................................
# execute consumer
CLASSNAME=org.apache.kafka.common.serialization.StringDeserializer
java -Dbootstrap.servers=kafka:9092 -Dgroup.id=khallware \
     -Dkey.deserializer=$CLASSNAME -Dvalue.deserializer=$CLASSNAME \
     -jar target/kafka-poc-0.0.1.jar

# ctrl-a n
# .............................................................................
# send a message to a topic
printf "{ 'key1' : 'value1' }\n" |kafka-console-producer.sh --broker-list localhost:9092 --topic khallware

# ctrl-a n
# .............................................................................
# notice that the java consumer has consumed the kafka message
```
