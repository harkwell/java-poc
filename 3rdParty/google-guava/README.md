Google Guava Library
=================
Overview
---------------
From website:

> Guava is a set of core libraries that includes new collection types (such as
> multimap and multiset), immutable collections, a graph library, functional
> types, an in-memory cache, and APIs/utilities for concurrency, I/O, hashing,
> primitives, reflection, string processing, and much more.

```shell
chromium-browser https://opensource.google.com/projects/guava
```
