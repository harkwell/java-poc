package com.khallware.jpatrx;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Main
{
	private static final Logger logger = LoggerFactory.getLogger(
		Main.class);

	private static EntityManager createEntityManager()
			throws IllegalStateException
	{
		EntityManager retval = Persistence
			.createEntityManagerFactory("poc")
			.createEntityManager();
		return(retval);
	}

	private static List<Item> searchByKey(String key, EntityManager em)
	{
		logger.debug("searching for item with key ({})", key);
		return(em.createQuery(""
				+"SELECT i "
				+  "FROM Item i "
				+ "WHERE i.key LIKE :itemKey")
			.setParameter("itemKey", key)
			.setMaxResults(10)
			.getResultList());
	}

	/**
	 * All arguments passed into main are transactional and an existing
	 * key will cause the transaction to rollback.
	 */
	public static void main(String[] args) throws Exception
	{
		boolean rollback = false;
		EntityManager em = Main.createEntityManager();
		Repo repo = new Repo();
		Item item = null;
		em.getTransaction().begin();

		for (String arg : args) {
			String[] row = arg.split(":");
			logger.info("creating new item... {}", row);
			item = new Item(row[0], row[1]);
			rollback |= (Main.searchByKey(row[0], em).size() > 0);
			logger.debug("created ({})", item);
			em.persist(item);
		}
		if (rollback) {
			logger.info("dups found, rolling back...");
			em.getTransaction().rollback();
		}
		else {
			logger.info("commit()ing data.");
			em.getTransaction().commit();
		}
		em.close();
	}
}
