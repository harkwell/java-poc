JPA Transactions
=================
Overview
---------------

```shell
chromium-browser https://en.wikibooks.org/wiki/Java_Persistence/Transactions
```

Build
---------------

```shell
git clone https://gitlab.com/harkwell/java-poc.git
cd java-poc/j2ee/jpa-persistence/transactions
POC_MAVEN_REPO=/tmp/foo
rm -rf $POC_MAVEN_REPO && mkdir -p $POC_MAVEN_REPO
mvn -Dmaven.repo.local=$POC_MAVEN_REPO package assembly:single
ls -ld target/jpatrx-poc-0.0.1-jar-with-dependencies.jar
```

Execute
---------------

```shell
export CLASSPATH=$POC_MAVEN_REPO/org/hsqldb/hsqldb/2.4.0/hsqldb-2.4.0.jar:$CLASSPATH

# start the database
java org.hsqldb.server.Server --database.0 file:test.db --dbname.0 test

# insert items into database with poc (Main.java)
java -jar target/jpatrx-poc-0.0.1-jar-with-dependencies.jar key1:val1 key2:val2
#   1) notice that both are persisted to the database
java -jar target/jpatrx-poc-0.0.1-jar-with-dependencies.jar key2:val2 key3:val3
#   2) notice that key2 is persisted once and key3 is not persisted at all
java -jar target/jpatrx-poc-0.0.1-jar-with-dependencies.jar key3:val3 key4:val4
#   3) notice that all four are persisted to the database

# test
henplus
connect jdbc:hsqldb:hsql://localhost:9001/test
SELECT * FROM item;
exit
```
